<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211121142549 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql(
            'ALTER TABLE user 
            ADD first_name VARCHAR(255) NULL, 
            ADD last_name VARCHAR(255) NULL, 
            ADD date_of_birth TIMESTAMP NULL,
            ADD created_at TIMESTAMP NOT NULL,
            ADD password_changed_at TIMESTAMP NOT NULL
        '
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `user` DROP first_name, DROP last_name, DROP date_of_birth, DROP created_at, DROP password_changed_at');
    }
}
