<?php

declare(strict_types=1);

namespace App\Controller;

use App\Mailer\Email;
use App\Mailer\Mailer;
use App\Mailer\Recipient;
use App\Mailer\Recipients;
use App\Mailer\Sender;
use App\Mailer\Templates;
use App\Password\PasswordGenerator;
use App\Service\UserService;
use App\Xls\Reader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ImportController extends AbstractController
{
    private $mailer;
    private $templates;

    public function __construct(Mailer $mailer, Templates $templates)
    {
        $this->mailer = $mailer;
        $this->templates = $templates;
    }

    public function import(Request $request, Reader $reader, UserService $userService): Response
    {
        if ('POST' === $request->getMethod()) {
            $file = $request->files->get('_file');

            $fileFolder = __DIR__.'/../../public/uploads/';

            $filePathName = \md5(\uniqid()).$file->getClientOriginalName();

            try {
                $file->move($fileFolder, $filePathName);
            } catch (FileException $e) {
                throw $e;
            }

            $rows = $reader->read($fileFolder.$filePathName);

            foreach ($rows as $row) {
                $password = PasswordGenerator::generate(8);

                $userService->createUser(
                    $row['firstName'],
                    $row['lastName'],
                    $row['email'],
                    new \DateTimeImmutable($row['dob']),
                    $password
                );

                $this->mailer->send(
                    new Email(
                        $subject = 'Welcome message with password.',
                        $this->templates->welcomeMessageWithPassword($password)
                    ),
                    new Sender('info@example.com', 'Info'),
                    new Recipients([
                        new Recipient($row['email'], $row['firstName'].' '.$row['lastName']),
                    ])
                );
            }

            $this->redirectToRoute('list');
        }

        return $this->render('index/import.html.twig');
    }
}
