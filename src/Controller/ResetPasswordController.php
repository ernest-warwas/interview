<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\ResetPasswordFormType;
use App\Repository\UserRepository;
use App\Security\PasswordEncoder;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ResetPasswordController extends AbstractController
{
    public function reset(
        Request $request,
        UserRepository $userRepository,
        PasswordEncoder $passwordEncoder,
        EntityManagerInterface $entityManager
    ): Response {
        $form = $this->createForm(ResetPasswordFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $userRepository->findOneBy(['email' => $form->get('email')->getData()]);

            if ($passwordEncoder->verify($user->getPassword(), $form->get('oldPlainPassword')->getData())) {
                $user->setPassword($passwordEncoder->encode($form->get('newPlainPassword')->getData()));

                $entityManager->persist($user);
                $entityManager->flush();

                return $this->redirectToRoute('login');
            }
        }

        return $this->render('user/reset_password.html.twig', [
            'resetPasswordForm' => $form->createView(),
        ]);
    }
}
