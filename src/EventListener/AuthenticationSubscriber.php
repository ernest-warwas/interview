<?php

declare(strict_types=1);

namespace App\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Event\AuthenticationSuccessEvent;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

final class AuthenticationSubscriber implements EventSubscriberInterface
{
    private $daysToResetPassword;

    public function __construct(int $daysToResetPassword)
    {
        $this->daysToResetPassword = $daysToResetPassword;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AuthenticationSuccessEvent::class => 'onAuthenticationSuccess',
        ];
    }

    public function onAuthenticationSuccess(AuthenticationSuccessEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        if ($user->getPasswordChangedAt()->diff(new \DateTimeImmutable())->days > $this->daysToResetPassword) {
            throw new AuthenticationException();
        }
    }
}
