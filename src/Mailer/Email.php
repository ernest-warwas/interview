<?php

declare(strict_types=1);

namespace App\Mailer;

final class Email
{
    private $subject;
    private $htmlContent;

    public function __construct(string $subject, string $htmlContent)
    {
        $this->subject = $subject;
        $this->htmlContent = $htmlContent;
    }

    public function subject(): string
    {
        return $this->subject;
    }

    public function htmlContent(): string
    {
        return $this->htmlContent;
    }
}
