<?php

declare(strict_types=1);

namespace App\Mailer;

interface Mailer
{
    public function send(Email $email, Sender $sender, Recipients $recipients): void;
}
