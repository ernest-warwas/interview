<?php

declare(strict_types=1);

namespace App\Mailer;

final class Recipient
{
    private $email;
    private $name;

    public function __construct(string $email, string $name = null)
    {
        $this->email = $email;
        $this->name = $name;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function name(): ?string
    {
        return $this->name;
    }
}
