<?php

declare(strict_types=1);

namespace App\Mailer;

final class Recipients extends \ArrayObject
{
    /**
     * @param Recipient[] $recipients
     */
    public function __construct(array $recipients)
    {
        parent::__construct($recipients);
    }
}
