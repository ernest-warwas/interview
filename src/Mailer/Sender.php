<?php

declare(strict_types=1);

namespace App\Mailer;

final class Sender
{
    private $email;
    private $name;

    public function __construct(string $email, string $name)
    {
        $this->email = $email;
        $this->name = $name;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function withOverwrittenEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function withOverwrittenName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
