<?php

declare(strict_types=1);

namespace App\Mailer;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email as MailerEmail;

final class SymfonyMailer implements Mailer
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function send(Email $email, Sender $sender, Recipients $recipients): void
    {
        $message = (new MailerEmail())
            ->from(new Address($sender->email(), $sender->name()))
            ->subject($email->subject())
            ->html($email->htmlContent());

        foreach ($recipients as $recipient) {
            /* @var Recipient $recipient */
            $message->addTo(new Address($recipient->email(), $recipient->name()));
        }

        $this->mailer->send($message);
    }
}
