<?php

declare(strict_types=1);

namespace App\Mailer;

interface Templates
{
    public function welcomeMessageWithPassword(string $password): string;
}
