<?php

declare(strict_types=1);

namespace App\Mailer;

use Twig\Environment as TwigEnvironment;

final class TwigTemplates implements Templates
{
    private $twigEnvironment;

    public function __construct(TwigEnvironment $twigEnvironment)
    {
        $this->twigEnvironment = $twigEnvironment;
    }

    public function welcomeMessageWithPassword(string $password): string
    {
        return $this->twigEnvironment->render('email/password.html.twig', [
            'password' => $password,
        ]);
    }
}
