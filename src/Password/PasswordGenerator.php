<?php

declare(strict_types=1);

namespace App\Password;

final class PasswordGenerator
{
    public static function generate(int $length): string
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?';

        return \mb_substr(\str_shuffle($chars), 0, $length);
    }
}
