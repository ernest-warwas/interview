<?php

declare(strict_types=1);

namespace App\Security;

interface PasswordEncoder
{
    public function encode(string $password): string;

    public function verify(string $hashed, string $plain): bool;
}
