<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

final class SymfonyPasswordEncoder implements PasswordEncoder
{
    private $hasher;

    public function __construct(PasswordHasherFactoryInterface $hasher)
    {
        $this->hasher = $hasher->getPasswordHasher(User::class);
    }

    public function encode(string $password): string
    {
        return $this->hasher->hash($password);
    }

    public function verify(string $hashed, string $plain): bool
    {
        return $this->hasher->verify($hashed, $plain);
    }
}
