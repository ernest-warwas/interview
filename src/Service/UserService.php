<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Security\PasswordEncoder;
use Doctrine\ORM\EntityManagerInterface;

final class UserService
{
    private $entityManager;
    private $passwordEncoder;

    public function __construct(
        EntityManagerInterface $entityManager,
        PasswordEncoder $passwordEncoder
    ) {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    public function createUser(
        string $firstName,
        string $lastName,
        string $email,
        \DateTimeImmutable $dateOfBirth,
        string $password
    ): void {
        $user = new User();
        $user->setEmail($email);
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setDateOfBirth($dateOfBirth);
        $user->setPassword($this->passwordEncoder->encode($password));
        $user->setPasswordChangedAt(new \DateTimeImmutable());
        $user->setCreatedAt(new \DateTimeImmutable());

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }
}
