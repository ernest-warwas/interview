<?php

declare(strict_types=1);

namespace App\Xls;

use PhpOffice\PhpSpreadsheet\Reader\Xls;

final class PhpOfficeXlsReader implements Reader
{
    private $xls;

    public function __construct()
    {
        $this->xls = new Xls();
    }

    public function read(string $filename): array
    {
        $this->xls->setReadEmptyCells(false);
        $this->xls->setReadDataOnly(true);
        $spreadsheet = $this->xls->load($filename)->getActiveSheet();
        $spreadsheet = $spreadsheet->toArray();

        $headers = \array_shift($spreadsheet);
        \array_walk(
            $spreadsheet,
            function (array &$row) use ($headers) {
                $row = \array_combine($headers, $row);
            }
        );

        return $spreadsheet;
    }
}
