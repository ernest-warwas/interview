<?php

declare(strict_types=1);

namespace App\Xls;

interface Reader
{
    public function read(string $filename): array;
}
